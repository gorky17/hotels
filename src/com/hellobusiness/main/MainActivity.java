package com.hellobusiness.main;

import android.app.Activity;
import android.os.Bundle;
import com.hellobusiness.fragments.HotelHomeFragment;
import com.hellobusiness.hotels.R;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, new HotelHomeFragment()).commit();

	}
}
