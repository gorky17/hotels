package com.hellobusiness.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ContentFetcher;
import com.hellobusiness.utilities.TagHandler;

public class OffersFragment extends Fragment {

	protected static final String TAG = "OffersFragment";
	private static RelativeLayout parent;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final ScrollView layout = (ScrollView) inflater.inflate(
				R.layout.offers_fragment, container, false);
		parent = (RelativeLayout) layout.findViewById(R.id.relLayoutOffers);//TODO maybe linear layout? and how to put footer then?
		// get all the offers, put them in a list
		// for every offer create a text view

		final ContentFetcher fetcher = new ContentFetcher();

		try {
			// show spinners here (eg. loading)
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setTitle("Please wait");
			dialog.setMessage("Getting content");
			dialog.setIndeterminate(true);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.show();

			final HashMap<String, Object> itemsReturned = fetcher.execute(
					"http://rodosseavillas.gr/wp-json/posts/310").get();

			Log.d(TAG, itemsReturned.get("text").toString());

			Thread loadData = new Thread(new Runnable() {

				@Override
				public void run() {
					// for every offer returned create a text field, set its
					// text to the offer and put it to the parent
					Document document = Jsoup.parse(itemsReturned.get("text")
							.toString());
					final Elements elements = document.select("p");
					Log.d(TAG, "number of offers: " + elements.size());
					List<TextView> listOffers = new ArrayList<TextView>();

					for (int i = 0; i < elements.size(); i++) {
						Log.d(TAG, "Loop no: " + i);
						final TextView textView = new TextView(getActivity());
						RelativeLayout.LayoutParams textViewLayoutParams = new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.MATCH_PARENT,
								RelativeLayout.LayoutParams.WRAP_CONTENT);
						textViewLayoutParams.setMargins(10, 10, 10, 10);
						textView.setBackgroundResource(R.layout.offers_background);
						textView.setPadding(10, 10, 10, 10);
						// if i=0 set it on top, else stack everything below
						if (i == 0) {
							textViewLayoutParams
									.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						} else {
							int childAboveid = listOffers.get(i - 1).getId();
							textViewLayoutParams.addRule(RelativeLayout.BELOW,
									childAboveid);
						}
						textView.setLayoutParams(textViewLayoutParams);
						Element element = elements.get(i);

						SpannableStringBuilder stringBuilder;
						stringBuilder = ((SpannableStringBuilder) Html
								.fromHtml((String) element.toString(), null,
										new TagHandler()));

						textView.setText(stringBuilder);
						textView.setId(i);
						listOffers.add(textView);
					}

					actionComplete(dialog, listOffers);

				}
			});
			loadData.start();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return layout;
	}

	// @Override
	public void actionComplete(final Dialog dialog,
			final List<TextView> textView) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dialog.dismiss();
				for (int i = 0; i < textView.size(); i++) {
					parent.addView(textView.get(i));
				}
			}
		});
	}
}
