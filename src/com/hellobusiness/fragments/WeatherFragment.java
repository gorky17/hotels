package com.hellobusiness.fragments;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import javax.net.ssl.HttpsURLConnection;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ImageGetter;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WeatherFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final RelativeLayout layout = (RelativeLayout) inflater.inflate(
				R.layout.weather_fragment, container, false);

		Thread weatherFetcher = new Thread(new Runnable() {

			@Override
			public void run() {
				ArrayList<Object> temp = getRodosTemp();
				if (temp.size() != 0) {
					ImageView weatherImage = (ImageView) layout
							.findViewById(R.id.weatherImage);
					weatherImage.setImageDrawable((Drawable) temp.get(0));

					TextView weatherTemp = (TextView) layout
							.findViewById(R.id.weatherTemp);
					weatherTemp.setText(((Integer) temp.get(1)).toString());
				}
			}
		});
		weatherFetcher.start();
		try {
			weatherFetcher.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return layout;
	}

	/**
	 * Returns an ArrayList that has a drawable on [0] and an integer on [1]
	 * */
	private ArrayList<Object> getRodosTemp() {
		String endpoint = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Rhodes%2C%20Gr%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys%22";
		int averageCelciusDegrees = 0;
		ArrayList<Object> temperatureImage = new ArrayList<Object>();
		try {
			URL endpointURL = new URL(endpoint);
			HttpsURLConnection connection = (HttpsURLConnection) endpointURL
					.openConnection();
			connection.connect();
			InputStream input = connection.getInputStream();
			JsonReader reader = new JsonReader(new InputStreamReader(input));
			reader.setLenient(true);
			JsonObject forecast = new JsonParser().parse(reader)
					.getAsJsonObject();
			JsonPrimitive image = forecast.getAsJsonObject("query")
					.getAsJsonObject("results").getAsJsonObject("channel")
					.getAsJsonObject("item").getAsJsonPrimitive("description");
			ImageGetter imgGetter = new ImageGetter(getActivity()) {
				@Override
				public Drawable getDrawable(String source) {
					Drawable image = null;// assign a system pic
					try {

						String[] builder = source.split("\"");
						StringBuilder source2 = new StringBuilder();
						for (int i = 1; i < builder.length - 1; i++) {
							source2.append(builder[i]);
						}
						Log.d("source",
								source2.substring(0, source2.length() - 1)
										.toString());
						URL sourceURL = new URL(source2.substring(0,
								source2.length() - 1).toString());

						URLConnection urlConnection = sourceURL
								.openConnection();
						urlConnection.connect();
						InputStream inputStream = urlConnection
								.getInputStream();
						BufferedInputStream bufferedInputStream = new BufferedInputStream(
								inputStream);
						Bitmap bm = BitmapFactory
								.decodeStream(bufferedInputStream);

						// convert Bitmap to Drawable
						image = new BitmapDrawable(context.getResources(), bm);
						image.setBounds(0, 0, bm.getWidth(), bm.getHeight());
						tempImage = image;
					} catch (IOException e) {
						e.printStackTrace();
					}
					return image;
				}
			};
			Html.fromHtml(image.toString(), imgGetter, null);
			temperatureImage.add(imgGetter.tempImage);

			JsonObject weather = forecast.getAsJsonObject("query")
					.getAsJsonObject("results").getAsJsonObject("channel")
					.getAsJsonObject("item").getAsJsonArray("forecast").get(0)
					.getAsJsonObject();

			int high = Integer.parseInt(weather.get("high").toString()
					.replace("\"", ""));
			int low = Integer.parseInt(weather.get("low").toString()
					.replace("\"", ""));
			averageCelciusDegrees = (((high + low) / 2) - 32) * 5 / 9;
			Log.d("Common nominal", "Average celcius: "
					+ (((high + low) / 2) - 32) * 5 / 9);

			temperatureImage.add(averageCelciusDegrees);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return temperatureImage;
	}

}
