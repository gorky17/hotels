package com.hellobusiness.fragments;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ContentFetcher;
import com.hellobusiness.utilities.ImageGetter;
import com.hellobusiness.utilities.TagHandler;
import com.hellobusiness.utilities.TaskCompleted;

public class HotelPresentationFragment extends Fragment implements
		TaskCompleted {

	static final String TAG = "HotelPresentationFragment";
	private SpannableStringBuilder stringBuilder;
	private List<Drawable> imagesContainer = new ArrayList<Drawable>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final ScrollView layout = (ScrollView) inflater.inflate(
				R.layout.hotel_presentation_fragment, container, false);

		// get text elements
		// get pictures and put them in place
		final ContentFetcher fetcher = new ContentFetcher();
		final ImageGetter imageGetter = new ImageGetter(getActivity()) {
			int counter = 0;

			@SuppressWarnings("deprecation")
			@Override
			public Drawable getDrawable(String source) {

				Drawable image = null;
				Drawable mockImage = null;
				try {
					URL sourceURL = new URL(source);
					URLConnection urlConnection = sourceURL.openConnection();
					urlConnection.connect();
					InputStream inputStream = urlConnection.getInputStream();
					BufferedInputStream bufferedInputStream = new BufferedInputStream(
							inputStream);
					Bitmap bm = BitmapFactory.decodeStream(bufferedInputStream);

					// convert Bitmap to Drawable
					if (counter == 0) {
						image = new BitmapDrawable(
								getActivity().getResources(), bm);
						image.setBounds(0, 0, bm.getWidth(), bm.getHeight());
					} else {
						image = new BitmapDrawable(
								getActivity().getResources(),
								Bitmap.createScaledBitmap(bm, 250, 250, true));
					}

					counter++;
				} catch (IOException e) {
					e.printStackTrace();
				}
				imagesContainer.add(image);
				mockImage = new BitmapDrawable();

				return mockImage;
			}
		};

		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setTitle("Please wait");
		dialog.setMessage("Getting content");
		dialog.setIndeterminate(true);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.show();

		try {
			final HashMap<String, Object> itemsReturned = fetcher.execute(
					"http://rodosseavillas.gr/wp-json/posts/204").get();

			final TextView mainTextView = (TextView) layout
					.findViewById(R.id.hotelPresentationTextView);
			Thread loadData = new Thread(new Runnable() {

				@SuppressWarnings("deprecation")
				@Override
				public void run() {
					stringBuilder = ((SpannableStringBuilder) Html.fromHtml(
							(String) itemsReturned.get("text"), imageGetter,
							new TagHandler()));

					// fill the ImageViews with images from imagesContainer
					/*
					 * imageGetter.getDrawable(itemsReturned.get("text")
					 * .toString());
					 */
					// check if list empty (insert a mock cap
					Log.d(TAG,
							"imagesContainer has images: "
									+ imagesContainer.size());
					for (int i = 0; i < imagesContainer.size(); i++) {
						// set the top picture
						if (i == 0) {
							final ImageView topImage = (ImageView) layout
									.findViewById(R.id.hotelPresentationImageView);
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									topImage.setBackgroundDrawable(imagesContainer
											.get(0));

								}
							});
							// set all the other pics
						} else {
							// if NOT true, put the image on the left, else
							// right
							if (i % 2 != 0) {
								final LinearLayout container = (LinearLayout) layout
										.findViewById(R.id.imagesColumnLeft);
								final ImageView image = new ImageView(
										getActivity());

								image.setBackgroundDrawable(imagesContainer
										.get(i));
								getActivity().runOnUiThread(new Runnable() {

									@Override
									public void run() {
										container.addView(image);
									}
								});
								Log.d(TAG, "INSERTING IMAGE");
							} else {
								final LinearLayout container = (LinearLayout) layout
										.findViewById(R.id.imagesColumnRight);
								final ImageView image = new ImageView(
										getActivity());
								image.setBackgroundDrawable(imagesContainer
										.get(i));

								getActivity().runOnUiThread(new Runnable() {

									@Override
									public void run() {
										container.addView(image);
									}
								});
								Log.d(TAG, "INSERTING IMAGE");
							}
						}
					}

					actionComplete(dialog, mainTextView);
				}
			});
			loadData.start();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return layout;
	}

	@Override
	public void actionComplete(final Dialog dialog, final View textView) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dialog.dismiss();
				Log.d(TAG, stringBuilder.toString());
				((TextView) textView).setText(stringBuilder);
			}
		});
	}

}
