package com.hellobusiness.fragments;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.api.client.extensions.android2.AndroidHttp;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.EmailUtilities;

public class ReceptionFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final RelativeLayout layout = (RelativeLayout) inflater.inflate(
				R.layout.reception_fragment, container, false);
		final EditText surname = (EditText) layout
				.findViewById(R.id.surnameTextView);
		final EditText firstName = (EditText) layout
				.findViewById(R.id.firstNameTextView);
		final EditText room = (EditText) layout.findViewById(R.id.room);
		final EditText message = (EditText) layout
				.findViewById(R.id.yourMessageTextView);
		final ImageButton sendButton = (ImageButton) layout
				.findViewById(R.id.sendEmail);
		sendButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (surname.getText().toString().length() > 0
						&& firstName.getText().toString().length() > 0
						&& room.getText().toString().length() > 0
						&& message.getText().toString().length() > 0) {
					// TODO hotel's email must be set here
					final MimeMessage email = EmailUtilities.createEmail(
							"george.daramouskas@targetintegration.com",
							"gorkygd67@gmail.com",
							"Client's message from mobile application", message
									.getText().toString());

					new Thread(new Runnable() {

						@Override
						public void run() {
							try {
								Message message = EmailUtilities
										.createMessageWithEmail(email);
								// TODO a email client must be created in order
								// to send
								// emails on behalf of the app
								EmailUtilities.sendMessage(new Gmail(
										AndroidHttp.newCompatibleTransport(),
										new GsonFactory(), null),
										"gorkygd67@gmail.com", email);

							} catch (MessagingException | IOException e) {
								e.printStackTrace();
							}
						}
					}).start();

				}

			}
		});

		return layout;
	}
}
