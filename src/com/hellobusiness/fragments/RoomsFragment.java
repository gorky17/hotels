package com.hellobusiness.fragments;

import com.hellobusiness.hotels.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class RoomsFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		RelativeLayout layout = (RelativeLayout) inflater.inflate(
				R.layout.room_fragment, container, false);

		return layout;
	}

}
