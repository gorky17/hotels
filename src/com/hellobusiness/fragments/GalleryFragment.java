package com.hellobusiness.fragments;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ContentFetcher;
import com.hellobusiness.utilities.ImageGetter;
import com.hellobusiness.utilities.TagHandler;
import com.hellobusiness.utilities.TaskCompleted;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class GalleryFragment extends Fragment implements TaskCompleted {
	private static final String TAG = "Gallery";
	private List<Drawable> imagesContainer = new ArrayList<Drawable>();
	private SpannableStringBuilder stringBuilder;

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final ScrollView layout = (ScrollView) inflater.inflate(
				R.layout.gallery_layout, container, false);
		final ContentFetcher fetcher = new ContentFetcher();
		final ImageGetter imageGetter = new ImageGetter(getActivity()) {
			int counter = 0;

			@Override
			public Drawable getDrawable(String source) {

				Drawable image = null;
				Drawable mockImage = null;
				try {
					URL sourceURL = new URL(source);
					URLConnection urlConnection = sourceURL.openConnection();
					urlConnection.connect();
					InputStream inputStream = urlConnection.getInputStream();
					BufferedInputStream bufferedInputStream = new BufferedInputStream(
							inputStream);
					Bitmap bm = BitmapFactory.decodeStream(bufferedInputStream);

					image = new BitmapDrawable(getActivity().getResources(),
							Bitmap.createScaledBitmap(bm, 250, 250, true));

					counter++;
				} catch (IOException e) {
					e.printStackTrace();
				}
				imagesContainer.add(image);
				mockImage = new BitmapDrawable();

				return mockImage;
			}
		};

		final ProgressDialog dialog = new ProgressDialog(getActivity());
		dialog.setTitle("Please wait");
		dialog.setMessage("Getting content");
		dialog.setIndeterminate(true);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.show();

		try {
			final HashMap<String, Object> itemsReturned = fetcher.execute(
					"http://rodosseavillas.gr/wp-json/posts/16").get();

			Thread loadData = new Thread(new Runnable() {

				@Override
				public void run() {
					stringBuilder = ((SpannableStringBuilder) Html.fromHtml(
							(String) itemsReturned.get("text"), imageGetter,
							new TagHandler()));
					Log.d(TAG,
							"imagesContainer has images: "
									+ imagesContainer.size());
					for (int i = 0; i < imagesContainer.size(); i++) {

						// if NOT true, put the image on the left, else
						// right
						if (i % 2 != 0) {
							final LinearLayout container = (LinearLayout) layout
									.findViewById(R.id.galleryColumnLeft);
							final ImageView image = new ImageView(getActivity());

							image.setBackgroundDrawable(imagesContainer.get(i));
							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									container.addView(image);
								}
							});
							Log.d(TAG, "INSERTING IMAGE");
						} else {
							final LinearLayout container = (LinearLayout) layout
									.findViewById(R.id.galleryColumnRight);
							final ImageView image = new ImageView(getActivity());
							image.setPadding(10, 10, 10, 10);
							image.setBackgroundDrawable(imagesContainer.get(i));

							getActivity().runOnUiThread(new Runnable() {

								@Override
								public void run() {
									container.addView(image);
								}
							});
							Log.d(TAG, "INSERTING IMAGE");
						}
					}

					actionComplete(dialog, null);
				}
			});
			loadData.start();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return layout;
	}

	@Override
	public void actionComplete(Dialog dialog, View view) {
		dialog.dismiss();
	}

}
