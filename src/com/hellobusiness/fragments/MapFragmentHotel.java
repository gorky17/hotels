package com.hellobusiness.fragments;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ContentFetcher;

public class MapFragmentHotel extends Activity implements OnMapReadyCallback {

	private static final String TAG = "MapFragmentHotel";
	private ContentFetcher fetcher = new ContentFetcher();
	private long lat, lon;
	private SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
	private HashMap<String, Object> results = new HashMap<String, Object>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.map_fragment);
		final String mapUrl = "http://rodosseavillas.gr/wp-json/posts/311";
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					getCoordinates(mapUrl);
				} catch (IOException e) {
					e.printStackTrace();
				}					
			}
		}).start();

		MapFragment mapFragment = (MapFragment) getFragmentManager()
				.findFragmentById(R.id.mapView);
		mapFragment.getMapAsync(this);

	}

	@Override
	public void onMapReady(GoogleMap map) {
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(41.889,
				-87.622), 16));

		// You can customize the marker image using images bundled with
		// your app, or dynamically generated bitmaps.
		map.addMarker(new MarkerOptions()
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.location))
				.anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
				.position(new LatLng(41.889, -87.622)));

	}

	private void getCoordinates(String mapUrl) throws IOException {
		URL endpointURL = new URL(mapUrl);
        HttpURLConnection connection = (HttpURLConnection) endpointURL
                .openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();
        JsonReader reader = new JsonReader(new InputStreamReader(input));
        reader.setLenient(true);
		
		JsonElement mapFrame = new JsonParser().parse(reader)
				.getAsJsonObject().get("content");
		Document doc = Jsoup.parse(mapFrame.toString());
		String coordinates = doc.select("iframe").attr("src");//TODO get coordinates from client
		//String substring = StringUtils.substringBetween(coordinates, "ll");
		
		
		Log.d(TAG, coordinates);
		
		
		
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
