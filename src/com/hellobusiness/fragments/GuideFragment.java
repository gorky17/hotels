package com.hellobusiness.fragments;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hellobusiness.hotels.R;
import com.hellobusiness.utilities.ContentFetcher;
import com.hellobusiness.utilities.ImageGetter;
import com.hellobusiness.utilities.TagHandler;
import com.hellobusiness.utilities.TaskCompleted;

public class GuideFragment extends Fragment implements TaskCompleted {

	public static final String TAG = "GuideFragment";
	private SpannableStringBuilder stringBuilder;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ScrollView layout = (ScrollView) inflater.inflate(
				R.layout.guide_fragment, container, false);

		final ContentFetcher fetcher = new ContentFetcher();
		final ImageGetter imageGetter = new ImageGetter(getActivity());

		try {	
			// show spinners here (eg. loading)
			final ProgressDialog dialog = new ProgressDialog(getActivity());
			dialog.setTitle("Please wait");
			dialog.setMessage("Getting content");
			dialog.setIndeterminate(true);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.show();

			final HashMap<String, Object> itemsReturned = fetcher.execute(
					"http://rodosseavillas.gr/wp-json/posts/206").get();
			final TextView mainTextView = (TextView) layout
					.findViewById(R.id.hotelGuideTextView);
			Thread loadData = new Thread(new Runnable() {

				@Override
				public void run() {
					stringBuilder = ((SpannableStringBuilder) Html.fromHtml(
							(String) itemsReturned.get("text"), imageGetter,
							new TagHandler()));
					actionComplete(dialog, mainTextView);
				}
			});
			loadData.start();

		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}

		return layout;
	}

	@Override
	public void actionComplete(final Dialog dialog, final View txt) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dialog.dismiss();
				Log.d(TAG, stringBuilder.toString());
				((TextView)txt).setText(stringBuilder);
			}
		});
	}
}

