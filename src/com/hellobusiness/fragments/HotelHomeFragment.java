package com.hellobusiness.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.hellobusiness.hotels.R;

public class HotelHomeFragment extends Fragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		RelativeLayout fragmentView = (RelativeLayout) inflater.inflate(
				R.layout.hotel_home_fragment, container, false);
		FragmentManager manager = getFragmentManager();
		final FragmentTransaction transaction = manager.beginTransaction();

		Button hotelButton = (Button) fragmentView
				.findViewById(R.id.hotelButton);
		hotelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer,
								new HotelPresentationFragment())
						.addToBackStack(null).commit();

			}
		});
		Button guideButton = (Button) fragmentView
				.findViewById(R.id.guideButton);
		guideButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer, new GuideFragment())
						.addToBackStack(null).commit();

			}
		});

		Button mapButton = (Button) fragmentView.findViewById(R.id.mapButton);
		mapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * transaction.replace(R.id.fragmentContainer, new
				 * MapFragmentHotel()) .addToBackStack(null).commit();
				 */
				getActivity().startActivity(
						new Intent(getActivity(), MapFragmentHotel.class));

			}
		});

		Button receptionButton = (Button) fragmentView
				.findViewById(R.id.receptionButton);
		receptionButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer,
								new ReceptionFragment()).addToBackStack(null)
						.commit();

			}
		});

		Button roomsButton = (Button) fragmentView
				.findViewById(R.id.roomsButton);
		roomsButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer, new RoomsFragment())
						.addToBackStack(null).commit();

			}
		});

		Button galleryButton = (Button) fragmentView
				.findViewById(R.id.galleryButton);
		galleryButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				transaction
						.replace(R.id.fragmentContainer, new GalleryFragment())
						.addToBackStack(null).commit();

			}
		});

		Button offersButton = (Button) fragmentView
				.findViewById(R.id.offersButton);
		offersButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer, new OffersFragment())
						.addToBackStack(null).commit();

			}
		});

		Button weatherButton = (Button) fragmentView
				.findViewById(R.id.weatherButton);
		weatherButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer, new WeatherFragment())
						.addToBackStack(null).commit();

			}
		});

		Button calendarButton = (Button) fragmentView
				.findViewById(R.id.calendarButton);
		calendarButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				transaction
						.replace(R.id.fragmentContainer, new CalendarFragment())
						.addToBackStack(null).commit();

			}
		});

		return fragmentView;
	}

}
