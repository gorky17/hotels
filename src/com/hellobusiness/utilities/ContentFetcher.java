package com.hellobusiness.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import android.os.AsyncTask;

public class ContentFetcher extends AsyncTask<String, Void, HashMap<String, Object>> {

	private HashMap<String, Object> itemsReturned = new HashMap<String, Object>();

	// TODO there should be a method that will be called
	// before the Fetcher to get the modify dates of our local elements and
	// compare them to the remote ones
	@Override
	protected HashMap<String, Object> doInBackground(String... URL) {

		try {
			URL hotelPresentationURL = new URL(URL[0]);
			HttpURLConnection conn = (HttpURLConnection) hotelPresentationURL
					.openConnection();
			conn.connect();
			InputStream reader = conn.getInputStream();
			JsonReader jsonReader = new JsonReader(
					new InputStreamReader(reader));
			jsonReader.setLenient(true);
			JsonResponse response = new Gson().fromJson(jsonReader,
					JsonResponse.class);

			// get response and re-create html
			String element = response.getContent();

			Document parsedDocument = Jsoup.parse(element);

			// extract the text from the HTML
			// TODO elements other than paragraphs or lists will not be
			// selected. Is there any way around this?
			Elements text = parsedDocument.select("p,li");

			String formattedString = text.toString()
					.replace("[one_half_first]", "")
					.replace("[/one_half_first]", "")
					.replace("[one_half_last]", "")
					.replace("[/one_half_last]", "");

			itemsReturned.put("text", formattedString);

			jsonReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return itemsReturned;
	}
}
