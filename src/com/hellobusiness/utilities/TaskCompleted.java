package com.hellobusiness.utilities;

import android.app.Dialog;
import android.view.View;
import android.widget.TextView;

public interface TaskCompleted {
	void actionComplete(final Dialog dialog, final View view);
}
