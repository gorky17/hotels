package com.hellobusiness.utilities;

import org.xml.sax.XMLReader;

import android.text.Editable;
import android.text.Html;
import android.util.Log;

/**
 * This class handles tags that the TextView does not support (lists etc.. )
 * 
 * */
public class TagHandler implements Html.TagHandler {
	boolean first = true;
	String parent = null;
	int index = 1;

	@Override
	public void handleTag(boolean opening, String tag, Editable output,
			XMLReader xmlReader) {
		Log.d(tag, "GOT UNKNOWN TAG");
		if (tag.equals("ul"))
			parent = "ul";
		else if (tag.equals("ol"))
			parent = "ol";
		if (tag.equals("li")) {
			if (parent.equals("ul")) {
				if (first) {
					output.append("\n\t•");
					first = false;
				} else {
					first = true;
				}
			} else {
				if (first) {
					output.append("\n\t" + index + ". ");
					first = false;
					index++;
				} else {
					first = true;
				}
			}
		}
	}
}
