package com.hellobusiness.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import android.util.Base64;

import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;

public class EmailUtilities {

	public static MimeMessage createEmail(String to, String from,
			String subject, String bodyText) {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage email = new MimeMessage(session);
		try {
			InternetAddress tAddress = new InternetAddress(to);
			InternetAddress fAddress = new InternetAddress(from);

			email.setFrom(fAddress);
			email.addRecipient(javax.mail.Message.RecipientType.TO, tAddress);
			email.setSubject(subject);
			email.setText(bodyText);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return email;
	}

	public static Message createMessageWithEmail(MimeMessage email)
			throws MessagingException, IOException {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		email.writeTo(bytes);
		String encodedEmail = Base64.encodeToString(bytes
				.toByteArray(), Base64.DEFAULT);
		Message message = new Message();
		message.setRaw(encodedEmail);
		return message;
	}

	public static void sendMessage(Gmail service, String userId,
			MimeMessage email) throws MessagingException, IOException {
		Message message = createMessageWithEmail(email);
		message = service.users().messages().send(userId, message).execute();

		System.out.println("Message id: " + message.getId());
	}

}
