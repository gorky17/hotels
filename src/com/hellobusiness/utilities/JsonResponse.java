package com.hellobusiness.utilities;

import java.io.Serializable;

public class JsonResponse implements Serializable {

	private static final long serialVersionUID = -8125568453376399843L;
	private String content;

	public JsonResponse() {
	}

	public JsonResponse(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
