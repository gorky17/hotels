package com.hellobusiness.utilities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;

/**
 * This class fetches the images from the parsed HTML. <b>It must be created in
 * a seperate thread from the UI thread for it performs network operations</b>
 * */
public class ImageGetter implements Html.ImageGetter {
	protected Context context;
	public Drawable tempImage;

	public ImageGetter(Context context) {
		this.context = context;
	}

	@Override
	public Drawable getDrawable(String source) {
		Drawable image = null;// assign a system pic
		try {
			URL sourceURL = new URL(source);
			URLConnection urlConnection = sourceURL.openConnection();
			urlConnection.connect();
			InputStream inputStream = urlConnection.getInputStream();
			BufferedInputStream bufferedInputStream = new BufferedInputStream(
					inputStream);
			Bitmap bm = BitmapFactory.decodeStream(bufferedInputStream);

			// convert Bitmap to Drawable
			image = new BitmapDrawable(context.getResources(), bm);
			image.setBounds(0, 0, bm.getWidth(), bm.getHeight());
			tempImage = image;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

}
